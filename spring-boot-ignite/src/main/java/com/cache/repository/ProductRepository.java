package com.cache.repository;

import com.cache.entity.Product;
import org.apache.ignite.springdata22.repository.IgniteRepository;
import org.apache.ignite.springdata22.repository.config.RepositoryConfig;

@RepositoryConfig(cacheName = "Product")
public interface ProductRepository extends IgniteRepository<Product, String> {
}
