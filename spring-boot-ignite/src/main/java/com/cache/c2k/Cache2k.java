package com.cache.c2k;

import com.cache.entity.Product;
import com.cache.repository.ProductRepository;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.cache2k.integration.FunctionalCacheLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Configuration
public class Cache2k {

    @Autowired
    ProductRepository productRepository;

    @Bean
    public Cache<Integer, Product> cache() {
        Cache<Integer, Product> cache = new Cache2kBuilder<Integer, Product>() {
        }
                .name("product")
                .entryCapacity(2000)
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .permitNullValues(true)
                .loader(new FunctionalCacheLoader<Integer, Product>() {
                    @Override
                    public Product load(Integer integer) throws Exception {
                        Optional<Product> product = productRepository.findById(String.valueOf(integer));
                        return product.get();
                    }
                })
                .build();

        return cache;
    }


}