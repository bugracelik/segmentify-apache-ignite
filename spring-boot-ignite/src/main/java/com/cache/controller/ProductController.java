package com.cache.controller;

import com.cache.entity.Product;
import com.cache.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping()
    public void createProduct() {
        productService.createProduct();
    }

    @GetMapping("/all")
    public Iterable<Product> getAllProduct() {
        return productService.findAll();
    }

    @PostMapping("/create_20000_product")
    public void createProduct_20000k() {
        productService.create_20000k_product();
    }

    @GetMapping("/count")
    public long countProduct() {
        return productService.count();
    }

    @GetMapping("/{id}")
    public Object findProductWithId(@PathVariable Integer id) throws JsonProcessingException {
        return productService.findById(id);
    }

    @GetMapping("/size/{size}")
    public ArrayList<Product> getData(@PathVariable Integer size) {
        return productService.findData(size);
    }

    @DeleteMapping("/all")
    public void delete() {
        productService.deleteAll();
    }

    @GetMapping("/random")
    public Product getRandomProduct(@RequestParam(value = "useLocalCache", required = false, defaultValue = "false") String useLocalCache, HttpServletResponse response) throws IOException {
        if (useLocalCache.equalsIgnoreCase("true"))
            return getRandomProductLocalCache();

        return productService.getRandomProduct();
    }

    @GetMapping("/random/localcache")
    public Product getRandomProductLocalCache() {
        return productService.getRandomProductWithLocalCache();
    }

    @GetMapping("cache")
    public String cache() {
        return productService.getCacheInfo();
    }


    @PutMapping("/updatedb/{id}/{name}")
    public void igniteUpdateName(@PathVariable Integer id, @PathVariable String name) {
        productService.igniteUpdateName(id, name);
    }

}
