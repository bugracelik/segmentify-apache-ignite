package com.cache.service;

import com.cache.entity.Product;
import com.cache.repository.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.ignite.IgniteCache;
import org.cache2k.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.cache.processor.EntryProcessor;
import javax.cache.processor.EntryProcessorException;
import javax.cache.processor.MutableEntry;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    Cache<Integer, Product> cache;

    @Autowired
    IgniteCache<String, Product> igniteCache;

    public void createProduct() {
        String id = String.valueOf(new Random().nextLong());
        productRepository.save(id, new Product());
    }

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public void create_20000k_product() {
        for (long i = 1; i <= 20000; i++) {
            String id = String.valueOf(i);
            productRepository.save(id, buildProduct(id));
        }
    }

    public long count() {
        return productRepository.count();
    }

    public Object findById(Integer id) throws JsonProcessingException {
        return cache.get(id); //add cache2k
    }

    public ArrayList<Product> findData(Integer size) {
        ArrayList<Product> arrayList = new ArrayList<Product>();

        for (long i = 1; i <= size; i++) {
            String id = String.valueOf(i);
            Optional<Product> productOptional = productRepository.findById(id);
            Product product = productOptional.get();
            arrayList.add(product);
        }

        return arrayList;
    }

    public void deleteAll() {
        productRepository.deleteAll();
    }


    public Product getRandomProduct() {
        int number = new Random().nextInt(20000) + 1;

        String id = String.valueOf(number);

        return productRepository.findById(id).get();

    }

    public Product getRandomProductWithLocalCache() {
        int number = new Random().nextInt(20000) + 1;
        return cache.get(number);
    }

    public String getCacheInfo() {
        return cache.toString();
    }

    public void igniteUpdateName(Integer id, String name) {
        String key = String.valueOf(id);

        igniteCache.invoke(key, new EntryProcessor<String, Product, Object>() {
            @Override
            public Object process(MutableEntry<String, Product> mutableEntry, Object... objects) throws EntryProcessorException {
                updateProduct(mutableEntry, name, id);
                return null;
            }
        });
        cache.remove(id);
    }

    private void updateProduct(MutableEntry<String, Product> mutableEntry, String name, Integer id) {
        Product product = mutableEntry.getValue();
        product.setName(name);
        mutableEntry.setValue(product);
    }

    private Product buildProduct(String id) {
        Product product = new Product();

        product.setProductId(id);
        product.setName("bilgisayar");
        product.setUrl("www.dell.com.tr");
        product.setImage("www.google.com/dell_bilgisayar");
        product.setPrice(345.23);
        product.setOldPrice(453.32);
        product.setCategoryList(getCategoryList());
        product.setBrand("dell");
        product.setInsertTime(new Timestamp(2323));
        product.setLastUpdateTime(new Timestamp(2311));
        product.setInStock(true);
        product.setGender("dell");
        product.setSizes(getSizeArrayList());
        product.setColors(getColorsArrayList());
        product.setParams(getParamsMap());

        return product;
    }

    private ArrayList<String> getSizeArrayList() {
        ArrayList<String> sizes = new ArrayList<String>();

        sizes.add("small");
        sizes.add("medium");
        sizes.add("large");

        return sizes;
    }

    private ArrayList<String> getColorsArrayList() {
        ArrayList<String> sizes = new ArrayList<String>();

        sizes.add("black");
        sizes.add("white");
        sizes.add("purple");

        return sizes;
    }

    private HashMap<String, String> getParamsMap() {
        HashMap<String, String> params = new HashMap<>();

        params.put("bugra", "iyi satıcı");
        params.put("tugberk", "iyi satıcı");

        return params;
    }

    private ArrayList<String> getCategoryList() {
        ArrayList<String> categoryList = new ArrayList<>();

        categoryList.add("teknoloji kategorisi");
        categoryList.add("indirim kategorisi");
        categoryList.add("pahalı eşya kategorisi");

        return categoryList;
    }
}
